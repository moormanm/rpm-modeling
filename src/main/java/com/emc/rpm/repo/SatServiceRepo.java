package com.emc.rpm.repo;
import com.emc.rpm.entity.SatService;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaRepository;

@RepositoryRestResource
public interface SatServiceRepo extends JpaRepository<SatService, Long> {

}
