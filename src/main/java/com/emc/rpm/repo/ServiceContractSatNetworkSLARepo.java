package com.emc.rpm.repo;
import com.emc.rpm.entity.ServiceContractSatNetworkSLA;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaRepository;

@RepositoryRestResource
public interface ServiceContractSatNetworkSLARepo extends JpaRepository<ServiceContractSatNetworkSLA, Long> {

}
