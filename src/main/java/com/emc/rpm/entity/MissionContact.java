package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class MissionContact {
	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	Long missionContactId;
	
	@ManyToOne
	Mission mission;
	
	@OneToOne
	MissionPartnerContact missionPartnerContact;

	@Column
	String role;
}
