package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Getter
@Setter
public class SatService {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long satServiceId;
	 
	 @ManyToOne
	 ServiceProvider serviceProvider; 
	 
	 @Column
	 String serviceName;
	 
	 @Column
	 String serviceType;
	 
	 @Column
	 String notes;
	 
	 //back edge relationships
	 @OneToMany(mappedBy="satService")
	 List<SatNetwork> satNetwork;
}
