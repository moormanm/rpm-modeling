package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class ServiceContractSatNetworkTerminalSLA {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long serviceContractSatNetworkTerminalSLAId;
	 
	 @OneToOne
	 ServiceContract serviceContract;
	 
	 @OneToOne
	 SatTerminal satTerminal; 
	 
	 @Column
	 String terminalSLA;
	 
	 @Column
	 Integer terminalPriority;
	 
	 @Column
	 Integer terminalUplinkMIR;
	 
	 @Column
	 Integer terminalUplinkCIR;
	 
	 @Column
	 Integer terminalDownlinkMIR;
	 
	 @Column
	 Integer terminalDownlinkCIR;
	 
	 @Column
	 Integer terminalTransferAllowedGB;
	 
	 @Column
	 Integer terminalTransferPeriodQty;
	 
	 @Column
	 String terminalTransferPeriodUnit;
	 
	 @Column
	 Integer terminalOnlineAllowedQty;
	 
	 @Column
	 String terminalOnlineAllowedUnit;
	 
	 @Column
	 Integer terminalOnlinePeriodQty;
	 
	 @Column
	 String terminalOnlinePeriodUnit;
	 

	 
}
