package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity
@Getter
@Setter
public class ServiceContractSatNetworkTermGroupSLA {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long serviceContractSatNetworkTermGroupSLAId;
	 
	 @OneToOne
	 SatNetwork serviceContractSatNetworkId; 
	 
	 @OneToOne
	 MissionTerminalGroup missionTerminalGroup;
	 
	 @Column
	 String networkSLA; 
	 
	 @Column
	 Integer groupPriority; 
	 
	 @Column
	 Integer groupUplinkMIR; 
	 
	 @Column
	 Integer groupUplinkCIR; 
	 
	 @Column
	 Integer groupDownlinkMIR;
	 
	 @Column
	 Integer groupDownlinkCIR;
	 
	 @Column
	 Integer groupTransferAllowedGB;
	 
	 @Column
	 Integer groupTransferPeriodQty;
	 
	 @Column
	 String groupTransferPeriodUnit;
	 
	 @Column
	 Integer groupOnlineAllowedQty;
	 
	 @Column
	 String groupOnlineAllowedUnit;
	 
	 @Column
	 Integer groupOnlinePeriodQty;
	 
	 @Column
	 String groupOnlinePeriodUnit;
	 

}
