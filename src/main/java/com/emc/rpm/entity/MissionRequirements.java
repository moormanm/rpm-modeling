package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class MissionRequirements {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	Long missionRequirementsId;

	@OneToOne
	Mission mission;
	
	@Column
	String coverageReqsGeoJSON;
	

}
