package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
@Getter
@Setter
public class ServiceContract {
	 @Id
	 @Column
     @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long serviceContractId;
	 
	
	 @ManyToOne
	 MissionPartner missionPartner;
	 
	 @ManyToOne
	 ServiceProvider serviceProvider;
	 
	 @Column
	 String serviceContractName;
	 
	 @Column
	 LocalDateTime serviceContractPoPStart;
	 
	 @Column
	 LocalDateTime serviceContractPoPEnd;
	 
	 @Column
	 String ServiceContractDescription;
	 
	 @Column
	 String serviceContractDocReference;
	 
	 @Column
	 String serviceContractTerms; 
	 
	 //Back edges
	 @OneToMany(mappedBy="serviceContract")
	 List<ServiceContractCircuit> serviceContractCircuits;
	 
	 @OneToOne(mappedBy="serviceContract")
	 ServiceContractSatNetworkSLA serviceContractSatNetworkSLA;
}
