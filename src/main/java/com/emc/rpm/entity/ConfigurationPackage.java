package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class ConfigurationPackage {
	
	 @Id
	 @Column
     @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long satTerminalConfigurationPackageId; 
	 
	 @ManyToOne
	 SatTerminal satTerminal;

	 @OneToOne
	 ServiceContract serviceContract; 
	 	 
	 @Column
	 Long revision;
	 
	 @Column
	 LocalDateTime revisionDate;
	 
	 @Column
	 LocalDateTime validityStartDate;
	 
	 @Column
	 LocalDateTime validityEndDate;
	 
	 @Column
	 String notes;
	 
	 @OneToMany(fetch = FetchType.LAZY)
	 List<DeviceConfig> deviceConfigs;
}

