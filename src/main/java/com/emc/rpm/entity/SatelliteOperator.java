package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Getter
@Setter
public class SatelliteOperator {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long satelliteOperatorId;
	 
	 @Column
	 String name;
	 
	 @Column
	 String contactInfo;
	 
	 //Back edge relationships
	 @OneToMany(mappedBy="satelliteOperator")
	 List<SatelliteSpacecraft> spacecraft;
}
