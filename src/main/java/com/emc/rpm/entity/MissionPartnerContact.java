package com.emc.rpm.entity;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class MissionPartnerContact {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long missionPartnerContactId; 
	 
	 @ManyToOne
	 MissionPartner missionPartner;
	 
	 @Column
	 String firstName;
	 
	 @Column
	 String lastName;
	 
	 @Column
	 String role;
	 
	 @Column
	 String officePhone;
	 
	 @Column
	 String mobilePhone;
	 
	 @Column
	 String title;
	 
	 @Column
	 String rank;
	 
	 @Column
	 String email;
	 
	 @Column
	 String officeStreetAddress;
	 
	 @Column
	 String notes;
}
