package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@RestResource
@Entity
@Getter
@Setter
public class DeviceConfig {
	 @Id
	 @Column
     @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long deviceConfigId;
	 
	 @ManyToOne(fetch=FetchType.LAZY)
	 ConfigurationPackage configurationPackage;
	 
	 @Column
	 String deviceModelString;
	 
	 @Column
	 String serialNumber;
	 
	 @Column
	 byte[] configurationFile;
	 
	 @Column
	 String softwareVersion; 
}
