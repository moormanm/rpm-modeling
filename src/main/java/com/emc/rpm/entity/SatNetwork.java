package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
@Getter
@Setter
public class SatNetwork {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long satNetworkId;
	 
	 @ManyToOne
	 ServiceProvider serviceProvider;
	 
	 @ManyToOne
	 SatService satService;
	 
	 @ManyToOne
	 SatelliteSpacecraft satelliteSpacecraft;
	 
	 @Column
	 String networkName;
	 
	 @Column
	 String coverageGeoJSON;
	 
	 @Column
	 String transPlanXML;
	 
	 @Column
	 Long uplinkBeamId;
	 
	 @Column
	 Long downlinkBeamId;
	 
	 @Column
	 String uplinkPol;
	 
	 @Column
	 String downlinkPol;

}
