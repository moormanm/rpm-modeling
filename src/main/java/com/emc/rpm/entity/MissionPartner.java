package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
@Getter
@Setter
public class MissionPartner {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long missionPartnerId;
	 
	 
	 @ManyToMany
	 List<Mission> missions;

	 @Column
	 String missionPartnerName;
	 
	 @Column
	 String serviceAgencyName;
	 
	 @Column
	 String notes;

	 //Back edges
	 @OneToMany(mappedBy="missionPartner")
	 List<MissionPartnerContact> missionPartnerContacts;
	 
	 @OneToMany(mappedBy="missionPartner")
	 List<ServiceContract> serviceContracts;

}
