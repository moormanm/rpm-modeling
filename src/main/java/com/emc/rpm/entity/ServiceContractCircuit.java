package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class ServiceContractCircuit {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long serviceContractCircuitId;
	 
	 @ManyToOne
	 ServiceContract serviceContract;
	 
	 @ManyToOne
	 SatLink satLink;

     @Column
	 String notes;
	
}
