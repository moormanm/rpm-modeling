package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.*;


@Entity
@Getter
@Setter
public class Mission {
	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	Long missionId;
	
	@Column
	String missionName;
	
	@Column
	String securityClassification;
	
	@Column
	LocalDateTime startDate;
	
	@Column
	LocalDateTime endDate;
	

	//Back Edges
	@ManyToMany(mappedBy="missions")
	List<MissionPartner> missionPartners;
	
	@OneToMany(mappedBy="mission")
	List<MissionContact> missionContact;
	
	@OneToMany(mappedBy="mission")
	List<MissionTerminalGroup> missionTerminalGroups;
	
	
}
