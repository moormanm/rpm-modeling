package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Getter
@Setter
public class SatelliteSpacecraft {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	Long satelliteSpacecraftId; 
	 	 
	 @ManyToOne
	 SatelliteOperator satelliteOperator;
	 
	 @Column
	 String satelliteName;
	 
	 @Column
	 String satelliteOrbit;
	 
	 @Column
	 String satelliteGeo; 
	 
	 //Back edges
	 @OneToMany(mappedBy="satelliteSpacecraft")
	 List<SatelliteSpacecraftBeam> satelliteSpacecraftBeams;
}

