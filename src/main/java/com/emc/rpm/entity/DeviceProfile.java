package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
@RestResource
@Entity
@Getter
@Setter
public class DeviceProfile {
	 @Id
	 @Column
     @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long deviceProfileId;
	 
	 @ManyToOne
	 SatTerminal satTerminal; 
	 
	 @Column
	 String equipmentType;
	 
	 @Column
	 String modelString;
	 
	 @Column
	 String serialNumber;
	 
	 
}
