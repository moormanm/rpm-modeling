package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class MissionTerminal {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long missionTerminalId;

	 @OneToOne
	 SatTerminal  satTerminal;
	 
	 @ManyToOne
	 MissionTerminalGroup missionTerminalGroup;
	 
	 @Column
	 String notes;
	 
	 @Column
	 Integer terminalPriority;
	 
	 @Column
	 Integer terminalUplinkMIR;
	 
	 @Column
	 Integer terminalUplinkCIR;
	 
	 @Column
	 Integer terminalDownlinkMIR;
	 
	 @Column
	 Integer terminalDownlinkCIR;
	 
	 @Column
	 Integer terminalTransferAllowedGB;
	 
	 @Column
	 Integer terminalTransferPeriodQty;
	 
	 @Column
	 String terminalTransferPeriodUnit;
	 
	 @Column
	 Integer terminalOnlineAllowedQty;
	 
	 @Column
	 String terminalOnlineAllowedUnit;
	 
	 @Column
	 Integer terminalOnlinePeriodQty;
	 
	 @Column
	 String terminalOnlinePeriodUnit;

	 
}
