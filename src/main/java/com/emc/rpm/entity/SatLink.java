package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class SatLink {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long satLinkId;
	 
	 @ManyToOne
	 ServiceProvider serviceProvider; 
	 
	 @OneToOne
	 SatelliteSpacecraft satellite;
	 
	 @Column
	 String transponder;
	 
	 @Column
	 String upPolarization;
	 
	 @Column
	 Integer upFreq;
	 
	 @Column
	 Integer upAllocBW;
	 
	 @Column
	 Integer upAllocPower;
	 
	 @Column
	 String dnPolarization;
	 
	 @Column
	 Integer dnFreq;
	 
	 @Column
	 boolean restricted;
	 
	 @Column
	 String modemModelString;
	 
	 @Column
	 String notes;
	 
	 //Back edges
     @OneToMany(mappedBy = "satLink")
	 List<ServiceContractCircuit> serviceContractCircuits;




}
