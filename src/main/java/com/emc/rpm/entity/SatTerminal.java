package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
@Getter
@Setter
public class SatTerminal {
	 @Id
	 @Column
     @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long satTerminalId;
	 
	 @Column
	 String terminalName;
	 
	 @OneToOne
	 MissionPartner missionPartner;
	 
	 @Column
	 String homeLocation;
	 
	 @Column
	 String notes;
	 
	 //Back edge relationships
	 @OneToOne(mappedBy="satTerminal")
	 MissionTerminal missionTerminal;
	 
	 @OneToMany(mappedBy="satTerminal")
	 List<DeviceProfile> deviceProfiles;
	 
	 @OneToMany(mappedBy="satTerminal")
	 List<ConfigurationPackage> configurationPackages;
	 
}
