package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Getter
@Setter
public class ServiceProvider {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long serviceProviderId;

	 @Column
	 String name;
	 
	 @Column
	 String pmContact;
	 
	 @Column
	 String opsContact;
	 
	 //Back edges
	 @OneToMany(mappedBy="serviceProvider")
	List<ServiceContract> serviceContracts;

	@OneToMany(mappedBy="serviceProvider")
	List<SatLink> satLinks;

	@OneToMany(mappedBy="serviceProvider")
	List<SatService> satServices;
}
