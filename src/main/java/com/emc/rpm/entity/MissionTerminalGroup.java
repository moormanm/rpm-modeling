package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class MissionTerminalGroup {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	Long missionTerminalGroupId;
	
	@ManyToOne
	Mission mission;
	
	@Column
	String missionGroupName;
	
	@Column
	String missionGroupDescription;
	
	@Column
	Integer groupPriority;
	
	@Column
	Integer groupUplinkMIR;
	
	@Column
	Integer groupUplinkCIR;
	
	@Column
	Integer groupDownlinkMIR;
	
	@Column
	Integer groupDownlinkCIR;
	
	@Column
	Integer groupTransferAllowedGB;
	
	@Column
	Integer groupTransferPeriodQty;
	
	@Column
	String groupTransferPeriodUnit;
	
	@Column
	Integer groupOnlineAllowedQty;
	
	@Column
	String GroupOnlineAllowedUnit;
	
	@Column
	Long groupOnlinePeriodQty;
	
	@Column
	String groupOnlinePeriodUnit;
	
	//Back edges
	@OneToMany(mappedBy="missionTerminalGroup")
	List<MissionTerminal> missionTerminals;
}
