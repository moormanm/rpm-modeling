package com.emc.rpm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity
@Getter
@Setter
public class ServiceContractSatNetworkSLA {
	 @Id
	 @Column
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 Long serviceContractSatNetworkSLA;
	 
	 @OneToOne
	 ServiceContract serviceContract;
	 
	 @OneToOne
	 SatNetwork satelliteNetwork;
	 
	 @Column
	 String networkTCPIdent;
	 
	 @Column
	 String	networkSLA;
	 
	 @Column
	 Integer networkUplinkMIR;
	 
	 @Column
	 Integer networkUplinkCIR; 
	 
	 @Column
	 Integer networkDownlinkMIR;
	 
	 @Column
	 Integer networkDownlinkCIR;
	 
	 @Column
	 Integer networkTransferAllowedGB;
	 
	 @Column
	 Integer networkTransferPeriodQty;
	 
	 @Column
	 String networkTransferPeriodUnit;
	 
	 @Column
	 Integer networkOnlineAllowedQty;
	 
	 @Column
	 String networkOnlineAllowedUnit;
	 
	 @Column
	 Integer networkOnlinePeriodQty;
	 
	 @Column
	 String networkOnlinePeriodUnit;
	 

	 
}
